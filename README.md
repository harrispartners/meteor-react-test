# Meteor + React Coding Test

Thanks for your interest in working with us! We've come up with this hopefully not very time consuming and interesting coding test (written by a developer who hates tests). Rather than a series of pedantic questions that have no real-world applicability, we think this is a better test of one's abilities.

Please follow the instructions below:

1. Clone this repository.
2. The app is intentionally in a non-working state. Go ahead and debug and fix any issues (hint: there are two).
3. After that, add the features described below.

## Features to Add

1. Make sure a user cannot save the same earthquake more than once (prevent duplicates).
2. Make the "delete" button on the saved list functional.
3. On the list of earthquakes, have the "save" button change to "unsave" if it's on the saved list. This will have the same effect as clicking "delete" on the saved list, it's just an alternate option for the user.

