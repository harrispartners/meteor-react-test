import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { SavedQuakes } from '/lib/collections/saved-quakes';

Meteor.methods({
  saveEarthquake(data) {
    check(data, {
      usgsId: String,
      title: String,
      magnitude: Number,
      when: Date,
    });

    return SavedQuakes.insert(data); // return new ID
  },
});
