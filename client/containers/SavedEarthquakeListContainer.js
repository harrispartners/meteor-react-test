import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { SavedQuakes } from '/lib/collections/saved-quakes';

import SavedEarthquakeList from '../components/SavedEarthquakeList';
export default createContainer(_ => {
  const handle = Meteor.subscribe('savedQuakes');
  const savedQuakes = SavedQuakes.find().fetch();

  return {
    dataReady: handle.ready(),
    savedQuakes,
  }
}, SavedEarthquakeList)
