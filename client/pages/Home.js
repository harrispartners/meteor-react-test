import React, { PropTypes } from 'react';
import EarthquakeList from '../components/EarthquakeList';
import SavedEarthquakeListContainer from '../containers/SavedEarthquakeListContainer';

export default Home = (props) => {
  return <div className="Home">
    <div className="Home__earthquake-list">
      <EarthquakeList />
    </div>

    <div className="Home__saved-earthquakes">
      <SavedEarthquakeListContainer />
    </div>
  </div>
}
