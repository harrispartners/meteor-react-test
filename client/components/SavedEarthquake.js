import React, { PropTypes } from 'react';

export default SavedEarthquake = ({ data }) => {
  return <div className="SavedEarthquake">
    <div className="SavedEarthquake__title">{data.title}</div>
    <div>
      <button>Delete</button>
    </div>
  </div>
}

SavedEarthquake.propTypes = {
  data: PropTypes.object.isRequired,
};
