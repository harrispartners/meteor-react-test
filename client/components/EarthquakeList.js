/*eslint-disable no-alert*/
import { Meteor } from 'meteor/meteor';
import React, { Component, PropTypes } from 'react';
import axios from 'axios';
import Earthquake from './Earthquake';

const reqUrl = 'http://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2016-04-01&endtime=2016-04-02&limit=50&eventtype=earthquake';

export default class EarthquakeList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      earthquakes: [],
      fetching: false,
    };
  }

  componentWillMount() {
    this.setState({ fetching: true });

    axios.get(reqUrl)
      .then(function(response) {
        if (response.status === 200) {
          this.setState({
            earthquakes: response.data.features,
            fetching: false,
          });
        }
      });
  }

  saveQuake(data) {
    Meteor.call('saveEarthquake', {
      usgsId: data.id,
      title: data.properties.title,
      magnitude: data.properties.mag,
      when: new Date(data.properties.time),
    }, (error, result) => {
      if (error) alert('Problem saving earthquake');
    });
  }

  render() {
    if (this.state.fetching) return <div>Loading earthquake data...</div>;

    const earthquakeList = this.state.earthquakes.map(quake => {
      return <Earthquake
        key={quake.id}
        data={quake}
        onSave={this.saveQuake}
      />
    });

    return <div>
      {earthquakeList}
    </div>
  }
}
