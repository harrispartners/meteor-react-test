import React, { PropTypes } from 'react';
import SavedEarthquake from './SavedEarthquake';

export default SavedEarthquakeList = (props) => {
  let savedQuakes;

  if (props.dataReady) {
    savedQuakes = props.savedQuakes.map(quake => {
      return <SavedEarthquake key={quake._id} data={quake} />
    });
  } else {
    savedQuakes = 'Loading...'
  }

  return <div className="SavedEarthquakeList">
    <h2>Saved Earthquakes</h2>
    {savedQuakes}
  </div>
}
