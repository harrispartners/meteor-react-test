/*eslint-disable no-alert*/

import React, { PropTypes } from 'react';
import moment from 'moment';

export default Earthquake = ({ data, onSave }) => {
  return <div className="Earthquake">
    <div className="Earthquake__details">
      <div className="Earthquake__location">
        {data.properties.place}
      </div>

      <div className="Earthquake__specifics">
        Magnitude: {data.properties.mag}<br />
        When: {moment(data.properties.time).format('YYYY-MM-DD h:mma')}<br />
        Full details: <a target="_blank" href={data.properties.url}>Click</a>
      </div>
    </div>

    <div className="Earthquake__controls">
      <button onTouchTap={_ => onSave(data)}>Save</button>
    </div>
  </div>
}

Earthquake.propTypes = {
  data: PropTypes.object.isRequired,
  onSave: PropTypes.func.isRequired,
};
